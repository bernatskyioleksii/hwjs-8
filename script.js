
// #1
//Об'єктна модель документа (DOM) є програмним інтерфейсом
// для HTML, XML і SVG документів.
//По суті вона пов'язує вебсторінка зі скриптами або мовами програмування.

// #2
//innerText витягує та встановлює вміст тега у вигляді простого тексту,
// тоді як innerHTML витягує та встановлює вміст у форматі HTML.

// #3
//document.createElement() - створює новий об'єкт Element
//document.getElementsByName() - повертає масив елементів за name
//document.getElementsByTagName() - масив елементів за назвою.
//document.getElementsByClassName() - повертає масив елементів за вказаним класом
//document.querySelector() - повертає об'єкт Element за вказаним CSS селектором.

// Мабуть, ці найкращі
//document.getElementById() - повертає об'єкт Element за його id **
//document.querySelectorAll() - повертає масив елементів за вказаним CSS селектором.**


// #1 Знайти всі параграфи на сторінці та встановити колір фону #ff0000

const findItems = document.querySelectorAll("p");

for (let element of findItems) {

	element.style.backgroundColor = '#ff0000';
}


// #2 Знайти елемент з id="optionsList". Вивести у консоль. Знайти батьківський елемент та вивести в консоль.
//Знайти дочірні ноди, якщо вони є, і вивести в консоль назви та тип нод.

 const findIdentifier = document.getElementById("optionsList");

   console.log(findIdentifier);

   // console.log(elem.parentElement);
    console.log(findIdentifier.parentNode);


const nodes = document.getElementById("optionsList").childNodes;

 for (let i of nodes) {
     console.log( i.nodeName);
     console.log( i.nodeType);
 }


// #3 Встановіть в якості контент елемента з класом testParagraph наступний параграф -
//This is a paragraph

const $elem = document.getElementById("testParagraph");
console.log($elem)
 $elem.innerHTML = '<p>This is a paragraph</p>'


// #4 Отримати елементи вкладені в елемент із класом main-header і вивести їх у консоль.
// Кожному з елементів присвоїти новий клас nav-item.

const findSelector = document.getElementsByClassName("main-header");

for (let element of findSelector) {
    console.log(element)
  element.classList.add("nav-item")
}


// #6 Знайти всі елементи із класом section-title. Видалити цей клас у цих елементів.

const findClass = document.querySelectorAll(".section-title");

for (let element of findClass) {
  element.classList.remove("section-title");
}



